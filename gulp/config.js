'use strict';

let gutil = require('gulp-util');

exports.paths = {
  src: 'src',
  assets: 'assets',
  dist: 'dist',
  tmp: '.tmp',
  npm: 'node_modules',
  routes: 'routes',
  controllers: 'controllers',
  libs: 'libs',
  models: 'models'
};

exports.errorHandler = () => {

  return (err) => {
    gutil.log(gutil.colors.red('[' + title + ']'), err.toString());
    this.emmit('end');
  };
};
