'use strict';

let modules = require('gulp-load-plugins')({
  DEBUG: false,
  pattern: ['gulp-*', 'gulp.*', 'vinyl-*'],
  scope: ['dependencies', 'devDependencies'],
  replaceString: /^gulp(-|\.)/,
  camelize: true,
  lazy: true,
  rename: {}
});

// console.log('Available modules', modules);

module.exports = modules;
