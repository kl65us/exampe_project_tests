'use strict';

let gulp = require('gulp');
let $ = require('./getPlugins');
let path = require('path');
let config = require('./config');
let del = require('del');

gulp.task('clean:tmp', () => {
  return del.sync([path.join(config.paths.tmp, '**/*')]);
});

gulp.task('clean:dist', () => {
  return del.sync([path.join(config.paths.dist, '**/*')]);
});

gulp.task('clean:assets:img', () => {
  return del.sync([path.join(config.paths.tmp, 'assets/images/**/*')]);
});

gulp.task('clean:assets:js', () => {
  return del.sync([path.join(config.paths.tmp, 'assets/assets.js')]);
});

gulp.task('clean:templates', () => {
  return del.sync([path.join(config.paths.tmp, 'templates/**/*')]);
});
