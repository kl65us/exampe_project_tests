'use strict';

let gulp = require('gulp');
let path = require('path');
let config = require('./config');
let $ = require('./getPlugins');
let pngquant = require('imagemin-pngquant');

gulp.task('images', () => {
  gulp.src(path.join(config.paths.assets, 'images/**/*'))
      // .pipe($.imagemin({
      //   progressive: true,
      //   svgoPlugins: [{ removeViewBox: false }],
      //   use: [pngquant()]
      // }))
      .pipe(gulp.dest(path.join(config.paths.tmp, 'images')));
});
