'use strict';

let gulp = require('gulp');
let browserify = require('browserify');
let $ = require('./getPlugins');
let config = require('./config');
let path = require('path');
let babelify = require('babelify');
let browserSync = require('browser-sync');
let plumber = require('gulp-plumber');

function bundle(bundler) {
  return bundler
          .bundle()
          .on('error', (err) => {
            $.util.log(err);
            browserSync.notify(err.message, 3000);
          })
          .pipe($.vinylSourceStream('app.js'))
          .pipe($.vinylBuffer())
          .pipe($.minify().on('error', $.util.log))
          .pipe($.uglify({mangle: false}).on('error', $.util.log))
          .pipe(gulp.dest(path.join(config.paths.tmp, 'app')));
}

gulp.task('index', () => {
  return gulp.src(path.join(config.paths.src, 'index.jade'))
        .pipe($.jade({
          client: false,
          pretty: false
        }))
        .on('error', (err) => {
          $.util.log(err);
        })
        .pipe(gulp.dest(config.paths.tmp));
});

gulp.task('bundle', () => {
    return browserify(path.join(config.paths.src, 'app.module.js')).transform("babelify", { presets: ['es2015', 'stage-0'] })
    .bundle()
    .pipe($.vinylSourceStream('app.js'))
    .pipe(gulp.dest(path.join(config.paths.tmp)));
});
