'use strict';

let gulp = require('gulp');
let browserify = require('browserify');
let $ = require('./getPlugins');
let config = require('./config');
let path = require('path');
let watchify = require('watchify');
let babelify = require('babelify');
let browserSync = require('browser-sync');
let plumber = require('gulp-plumber');

function bundle(bundler) {
  return bundler
          .bundle()
          .on('error', (err) => {
            $.util.log(err);
          })
          .pipe($.vinylSourceStream('app.js'))
          .pipe($.vinylBuffer())
          // .pipe($.minify().on('error', $.util.log))
          // .pipe($.uglify({mangle: false}).on('error', $.util.log))
          .pipe(gulp.dest(path.join(config.paths.tmp)));
}

gulp.task('watch', ['nodemon']);
// gulp.task('watch', ['nodemon'], () => {
//   gulp.watch(path.join(config.paths.assets, 'img/**/*'), ['clean:assets:img', 'images']);
//   gulp.watch(path.join(config.paths.assets, 'js/**/*'), ['clean:assets:js', 'assets:js']);
//   gulp.watch(path.join(config.paths.assets, 'less/**/*'), ['less']);
//   gulp.watch(path.join(config.paths.src, 'locales/**/*.json'), ['locales']);
//   gulp.watch(path.join(config.paths.src, '**/*.jade'), ['clean:templates', 'templates', 'index', $.livereload.reload]);
//   let watcher = watchify(browserify(path.join(config.paths.src, 'app.module.js')).transform("babelify", { presets: ['es2015', 'stage-0'] }), watchify.args);
//
//   bundle(watcher);
//   watcher.on('update', () => {
//     gulp.start(['index', 'lint']);
//     bundle(watcher);
//   });
//
//   watcher.on('log', $.util.log);
// });

gulp.task('templates', () => {
  gulp.src(path.resolve(config.paths.src, '**/*.jade'))
  .pipe($.jade({
    pretty: true,
    client: false
  }).on('error', (err) => {
    $.util.log(err);
  }))
  .pipe($.ignore.exclude(path.resolve(config.paths.src, 'index.html')))
  .pipe(gulp.dest(path.resolve(config.paths.tmp, 'templates')));
});

gulp.task('locales', () => {
  gulp.src(path.resolve(config.paths.src, 'locales/*.json'))
  .pipe(gulp.dest(path.resolve(config.paths.tmp, 'locales')));
});

gulp.task('nodemon', () => {
  return $.nodemon({
    script: 'server.js',
    tasks: ['lint:node']
  });
});

gulp.task('less', () => {
  gulp.src([
    path.join(config.paths.assets, 'less', 'meracl.less'),
    path.join(config.paths.assets, 'less', 'skins.less')
  ])
  .pipe($.less())
  .pipe($.cleanCss({ compatibility: 'ie8' }))
  .pipe(gulp.dest(path.join(config.paths.tmp, 'css')));
});

gulp.task('assets:js', () => {
  gulp.src(path.join(config.paths.assets, 'js/**/*.js'))
  // .pipe($.uglify({ magnle: false }).on('error', $.util.log))
  .pipe($.concat('assets.js'))
  .pipe(gulp.dest(path.join(config.paths.tmp)));
});

gulp.task('assets:plugins', () => {
  gulp.src(path.join(config.paths.assets, 'plugins/**/*'))
  .pipe(gulp.dest(path.join(config.paths.tmp, 'plugins')));
});
