'use strict';

let gulp = require('gulp');
let $ = require('./getPlugins');
let config = require('./config');
let path = require('path');

gulp.task('bootstrap', () => {
  gulp.src(path.join(config.paths.npm, 'bootstrap/fonts/*.{eot,svg,ttf,woff,woff2}'))
  .pipe(gulp.dest(path.join(config.paths.tmp)))
})
