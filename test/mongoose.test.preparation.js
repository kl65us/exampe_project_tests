'use strict';

let vars = require('../nodemon').env;

const _uri = `${vars.UNAME}:${vars.PASSWD}@${vars.HOST}:${vars.DB_PORT}/${vars.DATABASE}`;
const _options = { auth: { authdb: vars.AUTH_DB } };

module.exports = (mongoose) => (mongoose.createConnection( _uri, _options ));
