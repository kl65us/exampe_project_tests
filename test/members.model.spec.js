'use strict';

let expect = require('chai').expect;
let newMemberFullEnough = require('./fixtures/newMemberFullEnough');
let mongoose = require('mongoose');
let Members = require('../models/members');
let _instance = require('./mongoose.test.preparation')(mongoose);

describe( 'Member model', () => {
  let _id, _friendId;

  describe( 'Friends', () => {
    it( 'Should pass the test', done => {
      done();
    });
  });

  it( 'Should create simple member', done => {
    _instance.model('Member').createMember( newMemberFullEnough, (err, createdMember) => {
      if (err) return done(err);
      expect( createdMember ).to.be.an('object');
      expect( createdMember.toJSON() ).to.contain.all.keys ( newMemberFullEnough );

      //save _id for remove purpose after all tests
      _id = createdMember._id;

      done();
    });
  });

  it( 'Should return empty list of user friends', done => {
    _instance.model('Member').getAllFriends( _id , (err, doc) => {
      if (err) return done(err);
      expect( doc ).to.have.property('friends')
        .that.is.an('array')
        .that.have.lengthOf(0);
      done();
    });
  });

  it( 'Should add friend to user', done => {
    let createFriend = new Promise(( resolve, reject ) => {
      _instance.model('Member').createMember( newMemberFullEnough, (err, friend) => {
        if (err) return reject(err);
        expect( friend.toJSON() ).to.contain.all.keys( newMemberFullEnough );

        return resolve(friend._id);
      });
    });

    createFriend
      .then( id => _friendId = id )
      .then( friendId => {
        _instance.model('Member').addFriend( _id, friendId, (err, isUpdated) => {
          if (err) throw new Error(err);
          expect( isUpdated ).to.be.an('object')
            .with.property('friends')
            .that.is.an('array')
            .that.have.lengthOf(1);
          done();
        });
       })
      .catch( err => done(err));
  });

  it( 'Should return list of user friends', done => {
    _instance.model('Member').getAllFriends( _id , (err, doc) => {
      if (err) return done(err);
      expect( doc ).to.have.property('friends')
        .that.is.an('array')
        .that.have.lengthOf( 1 );
      expect( doc.friends[0].toJSON() ).to.contain.any.keys( newMemberFullEnough );

      done();
    });
  });

  it( 'Should remove friend from user friend list', done => {
    _instance.model('Member').removeFriend( _id, _friendId, err => {
      if (err) return done(err);
      _instance.model('Member').findById( _id, (err, member) => {
        if (err) return done(err);
        expect( member ).to.be.an('object')
          .that.have.property('friends')
          .that.is.an('array')
          .that.lengthOf(0);

          done();
      });
    });
  });

  after( done => {
    const condition = { $in: [_id, _friendId] };
    _instance.model('Member').remove( { _id: condition } , err => {
      if (err) return done(err);

      _instance.close();
      _instance.on( 'close', () => {
        done();
      });
    });
  });
});
