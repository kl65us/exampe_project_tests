'use strict';

let expect = require('chai').expect;
let img = require('lwip');

let app = require('../app');
let request = require('supertest');
let fs = require('fs');
let ms = require('../libs/sys/makeStream');
let path = require('path');

const _path = './uploads';
const _filename = 'testImage.png';

describe( 'Image uploading', () => {

  describe( 'Checking for upload destination', () => {
    it( 'Upload dir should exists', done => {
      fs.stat( _path, (err, stats) => {
        if (err || !stats) done(err);
        expect( stats.isDirectory() ).to.be.true;
        done();
      });
    });
  });

  describe( 'Creating image for test upload', () => {
    let _img = null;
    it( 'Should create image', done => {
      img.create( 2048, 2048, 'yellow', (err, image) => {
        if (err || !image) done(err);
        _img = image;

        expect(_img).to.be.an('object');
        expect(_img).to.exist;

        done();
      });
    });

    let _buf = null;
    it( 'Should create buffer from new image object', done => {
      _img.toBuffer( 'png', (err, buffer) => {
        if (err || !buffer) done(err);

        _buf = buffer;
        expect(_buf).to.exist;
        done();
      });
    });

    it( 'Should save test image for upload purposes', done => {
      let testImageStream = ms.createReadStream(_buf);

      let destImage = fs.createWriteStream( path.join(_path, _filename) );
      testImageStream.pipe( destImage );
      destImage.on('close', () => { done(); });
    });

    it( 'Should check does image exists', done => {
      fs.stat( path.join( _path, _filename ), ( err, stats ) => {
        if (err || !stats) done(err);
        expect( stats.isFile() ).to.be.true;
        done();
      });
    });
  });

  describe( 'Upload create image to API endpoint', () => {
    let endpointWithoutParams = '/api/uploads';
    let fullEndpoint = '/api/uploads?w=300&h=300&s=0.2&t=150&l=150&b=500&r=500';

    it( 'Should return error if there are no query params', done => {
      request(app)
        .post( endpointWithoutParams )
        .accept('application/vnd.api+json')
        .attach( 'avatar', path.join( _path, _filename ), 'testImage.png')
        .expect(404)
        .end(function(err, res) {
          expect( res.body.error.message ).to.be.a('string');
          done();
        });
    });

    it( 'Should return success response if image was saved', done => {
      request(app)
        .post( fullEndpoint )
        .accept('application/vnd.api+json')
        .attach( 'avatar', path.join( _path, _filename ), 'testImage.png')
        .expect(200)
        .end(function(err, res) {
          if (err) return done(err);
          expect( res.body.data.status ).to.be.true;

          let tempFilename = res.body.data.filename;
          expect( tempFilename ).to.be.a('string');

          fs.unlink( path.join( _path, tempFilename ), err => {
            if (err) return done(err);
            done();
          });
        });
    });
  });

  describe( 'Giving image from upload endpoint API', () => {
    const endpoint = `/api/uploads/${_filename}`;

    it( 'Should give image by specified name in url', done => {
      request(app)
        .get( endpoint )
        .expect( 'Content-Type', 'application/octet-stream')
        .expect( 'Content-disposition', `attachment; filename=${_filename}`)
        .expect(200, done);
    });
  });

  after( done => {
    fs.unlink( path.join( _path, _filename), err => {
      if (err) done(err);
      done();
    });
  });
});
