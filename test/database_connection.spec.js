'use strict';

let expect = require('chai').expect;
let mongoose = require('mongoose');
let Members = require('../models/members');
let _instance = null;

describe( 'Database testing', () => {

  //We put here assignment 'cause it happens too fast (connected etc)
  before( done => {
    _instance = require('./mongoose.test.preparation')(mongoose);
    done();
  });

  it( 'Should notify about successful connection to DB', done => {
    _instance.on( 'connected', () => {
      done();
    });
  });

  it( 'Should notify about connection close', done => {
    //closing connection to database
    _instance.close( () => {
    });

    _instance.on( 'close', () => {
      done();
    });
  });

});
