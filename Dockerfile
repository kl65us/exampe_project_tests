FROM node:6
MAINTAINER kl65us "efristical@gmail.com"
ENV REFRESHED_AT 2016-05-09

ENV DEBUG=exmaple_project:*
ENV NODE_ENV=production
ENV PORT=3001

ENV HOST 127.0.0.1
ENV DATABASE example_project_dev
ENV DB_PORT 27017
ENV UNAME klaus
ENV PASSWD klaus
ENV AUTH_DB admin

WORKDIR /usr/src/source
CMD ["node", "server.js"]
EXPOSE 3001
