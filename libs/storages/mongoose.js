'use strict';

let mongoose = require('mongoose');
let cf = require('konfig')();
let debug = require('debug')(`${cf.app.name}:mongoose`);

const uri = `${cf.mongo.username}:${cf.mongo.password}@${cf.mongo.host}:${cf.mongo.port}/${cf.mongo.db}`;

function init() {
  let options = {
    auth: { authdb: cf.mongo.authDB }
  };

  let instance = mongoose.connect(uri, options);

  instance.connection.on('connected', () => {
    debug('Mongoose is connected');
  });

  instance.connection.on('error', (err) => {
    debug('Mongoose error', uri, err.message);
  });

  instance.connection.on('disconnected', () => {
    debug('Mongoose disconnected');
  });

  process.on('SIGINT', () => {
    instance.connection.close(() => {
      debug('Mongoose is shutted down');
      process.exit(0);
    });
  });
}

module.exports.init = init;
