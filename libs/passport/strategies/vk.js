'use strict';

let passport = require('passport');
let passport_vk = require('passport-vkontakte');
let Members = require('../../../models/member');
let conf = require('konfig')();
let debug = require('debug')(`${conf.app.name}:vk_strategy`);

const VkStrategy = passport_vk.Strategy;

const strategy = new VkStrategy(
  {
    clientID: conf.passport.vk.appId,
    clientSecret: conf.passport.vk.appSecret,
    callbackURL:  `${conf.site.baseUrl}auth/vk/callback`
  },
  (accessToken, refreshToken, profile, done) => {
    Members.findOne({ 'social.id': profile.id }, (err, user) => {
      if (err) { done(err); }

      if (!user) {
        let username = profile.displayName.split(' ');
        //TODO: implement for member and teacher
        // Members.create({
        //   name: username[0],
        //   surname: username[1],
        //   social: {
        //     token: accessToken,
        //     id: profile.id,
        //     profileUrl: profile.profileUrl
        //   }
        // }, (err, user) => {
        //   if (err) done(err);
        //   done(null, user);
        // });
      }

      if (user) {
        done(null, user);
      }
    });
});

module.exports = strategy;
