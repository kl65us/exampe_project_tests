'use strict';

let passport = require('passport');
let passport_facebook = require('passport-facebook');
let Members = require('../../../models/members');
let conf = require('konfig')();
let debug = require('debug')(`${conf.app.name}:facebook_strategy`);

const FacebookStrategy = passport_facebook.Strategy;

const strategy = new FacebookStrategy(
  {
    clientID: conf.passport.facebook.appId,
    clientSecret: conf.passport.facebook.appSecret,
    callbackURL:  `${conf.site.baseUrl}auth/facebook/callback`
  },
  (accessToken, refreshToken, profile, done) => {
    //TODO: implement for teachers and members
    // Members.findOne({ 'social.id': profile.id }, (err, user) => {
    //   if (err) { done(err); }
    //
    //   if (!user) {
    //     let username = profile.displayName.split(' ');
    //
    //     Members.create({
    //       name: username[0],
    //       surname: username[1],
    //       social: {
    //         token: accessToken,
    //         id: profile.id,
    //         profileUrl: profile.profileUrl
    //       }
    //     }, (err, user) => {
    //       if (err) done(err);
    //       done(null, user);
    //     });
    //   }
    //
    //   if (user) {
    //     done(null, user);
    //   }
    // });
});

module.exports = strategy;
