'use strict';

let passport = require('passport');
let Members = require('../../models/members');

//get strategies
let local = require('./strategies/local');
let facebook = require('./strategies/facebook');
let vk = require('./strategies/vk');

//user preparation
passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

//apply strategies
passport.use('local', local);

//TODO: implement and apply another two strategies
