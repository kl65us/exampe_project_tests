'use strict';

const router = require('express').Router();
let members = require('./members');

router.use( '/members', members );

module.exports = router;
