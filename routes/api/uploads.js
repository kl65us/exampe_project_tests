'use strict';
const router = require('express').Router();
let multer = require('multer');
let uploads = require('../../controllers/api/upload.controllers');

let storage = multer.diskStorage({
  destination(req, file, cb) {
    cb( null, './uploads' );
  },

  filename(req, file, cb) {
    let filenameForUpload = `${Date.now()}${file.originalname}`;

    cb( null, filenameForUpload);
  }
});

let fileFilter = (req, file, cb) => {
  const acceptedFormats = new Set(['jpeg', 'png', 'gif']);

  const mimeRe = /\/(\w+)/i;
  let presentedFormat = mimeRe.exec( file.mimetype )[1];

  if ( acceptedFormats.has(presentedFormat) ) {
    cb( null, true);
  } else {
    let err = new Error('Unsupported format');
    err.code = 'LIMIT_UNSUPPORTED_FORMAT';
    cb( err );
  }
};

const limits = {
  fieldNameSize: 100,
  fieldSize: 1024 * 1024 * 1,
  fileSize: 1024 * 1024 * 5
};

let upload = multer({
  storage,
  fileFilter,
  limits
});

router.post( '/', upload.single('avatar'), uploads.upload );
router.get( '/:fileName', uploads.get );

module.exports = router;
