'use strict';

let accepts = require('accepts');
const debug = require('debug')('meracl:bootstrap');

module.exports = {
  validateAccepHeaders: validateAccepHeaders,
  setMetaInformation: setMetaInformation,
  setApiVersion: setApiVersion,
  setLinks: setLinks,
  setHeaders: setHeaders,
  constructJsonResponse: constructJsonResponse
};

function validateAccepHeaders(req, res, next) {
  const accept = accepts(req);
  if (accept.type(['application/vnd.api+json']) && accept.charset(['utf-8'])) {
    next();
  } else {
    next(new Error(`Client do not accept application/vnd.api+json or utf-8 charset.`));
  }
}

function setMetaInformation(req, res, next) {
  let meta = {
    author: 'meracl Development Team',
    email: 'hope@meracl.me'
  };

  res.locals.meta = meta;
  next();
}

function setApiVersion(req, res, next) {
  let version = '0.2';
  res.locals.jsonapi = {
    version
  };

  next();
}

function setLinks(req, res, next) {
  let currentPath = `${req.hostname}${req.originalUrl}`;
  res.locals.links = {
    self: currentPath
  };

  next();
}

function setHeaders(req, res, next) {
  let headers = {
    'Content-Type': 'application/vnd.api+json',
    'X-Powered-By': 'Hope',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PUTCH, HEAD',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  };

  res.set(headers);
  next();
}

function constructJsonResponse(req, res, next) {
  let parts = res.locals;
  let resultJson = {};

  for (var obj in parts) {
    resultJson[obj] = parts[obj];
  }

  res.locals.predefinedResponse = resultJson;
  next();
}
