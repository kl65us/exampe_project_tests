'use strict';

let mongoose = require('mongoose');
let Languages = require('./languages');
let authshop = require('./shared/authMember.shared');
let hashPassword = require('./shared/hashPassword.shared');

const oid = mongoose.Schema.Types.ObjectId;
const emailRegExp = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
const currency = ['USD', 'UAH', 'RUB', 'EUR'];

let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: false,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 },
  strict: false
};

const shopSchema = new mongoose.Schema({
  brandName: { type: String, maxlength: 30, required: true },
  foundedYear: { type: Date, required: false },
  location: { type: String, required: false },
  timezone: { type: Number, default: 0 },
  workEmail: { type: String, required: true, match: emailRegExp},
  //TODO: write upload functionality for this shit ;)
  poster: { type: String, required: false, default: '/uploads/default.png' },
  //TODO: how we will be changing user interface on front-end - using localStorage and this field?
  // I think it should be one entry not array with languages
  languages: Languages,
  //TODO: do we need payments for instant purchases?
  //I guess only users will buy clothes, not shops, but who nows,
  //I'll leave it here for a while
  payment: { type: mongoose.Schema.Types.Mixed, select: false },
  hash: { type: String, required: true, select: false },
  //TODO: edit notification types for shops
  notifications: [{ type: oid, ref: 'Notifications' }],
  is_deleted: { type: Boolean, required: true, default: false },
  created_at: { type: Date, required: true, default: new Date() },
  //TODO: we haven't online or offline func but we have last visit
  last_visit: { type: Date, required: true, default: new Date() },
  //TODO: what service we should use for currency exchange?
  currency: { type: String, enum: currency, default: 'USD', required: true },
  token: { type: String, required: false },
  // TODO: should be clear after 5 min
  //every shop can register by his own
  confirm_code: { type: String, required: false, select: false }
}, options);

//shop schema methods for convenient using and reusability
shopSchema.statics.hashPassword = hashPassword;
shopSchema.statics.authshop = authshop;

module.exports = mongoose.model('shop', shopSchema);
