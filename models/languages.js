let mongoose = require('mongoose');

const oid = mongoose.Schema.Types.ObjectId;

let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 },
  strict: true
};

const languageSchema = new mongoose.Schema({
  title: { type: String, required: true, minlength: 3 },
  code: { type: String, required: true, minlength: 2 }
});

module.exports = mongoose.model('Language', languageSchema);
