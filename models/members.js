'use strict';

let mongoose = require('mongoose');
let Languages = require('./languages');
let authMember = require('./shared/authMember.shared');
let hashPassword = require('./shared/hashPassword.shared');
let statics = require('./members.statics');

const oid = mongoose.Schema.Types.ObjectId;
const emailRegExp = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

//TODO: check does it works
const currency = require('./shared/currency');

let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 },
  strict: false
};

const memberSchema = new mongoose.Schema({
  firstname: { type: String, maxlength: 25, required: true },
  lastname: { type: String, maxlength: 25, required: false },
  birthday: { type: Date, required: false },
  location: { type: String, required: false },
  timezone: { type: Number, default: 0 },
  email: { type: String, required: true, match: emailRegExp},
  avatar: { type: String, required: false, default: '/uploads/default.png' },
  friends: [{ type: oid, ref: 'Member' }],
  //TODO: how we will be changing user interface on front-end - using localStorage and this field?
  languages: [ Languages ],
  //TODO: do we need payments for instant purchases? - I guess yes
  payment: { type: mongoose.Schema.Types.Mixed, select: false },
  hash: { type: String, required: true, select: false },
  //TODO: edit notification types for members
  notifications: [{ type: oid, ref: 'Notifications' }],
  is_deleted: { type: Boolean, required: true, default: false },
  created_at: { type: Date, required: true, default: new Date() },
  //TODO: we haven't online or offline func but we have last visit
  last_visit: { type: Date, required: true, default: new Date() },
  //TODO: what service we should use for currency exchange?
  currency: { type: String, enum: currency, default: 'USD', required: true },
  token: { type: String, required: false },
  // TODO: should be clear after 5 min
  confirm_code: { type: String, required: false, select: false },

  //to not count likes, outfits etc all the time
  likesCount: { type: Number, required: false, default: 0, select: false },
  outfitsCount: { type: Number, required: false, default: 0, select: false },
  friendsCount: { type: Number, required: false, default: 0, select: false },
  clothesCount: { type: Number, required: false, default: 0, select: false },

  //list of users that want to be your friend
  friendRequests: [ { type: mongoose.Types.ObjectId, ref: 'Member' } ]

}, options);

memberSchema.virtual('displayName')
 .get(function() {
    return `${this.firstname} ${this.lastname}`;
 })
 .set(function(name) {
    let fullName = name.split(' ');
    this.firstname = fullName[0];
    this.lastname = fullName[1];
 });

//TODO: somehow move these statics as below
//member schema statics for convenient using and reusability
memberSchema.statics.hashPassword = hashPassword;
memberSchema.statics.authMember = authMember;

//now we do like that
memberSchema.static( statics );

module.exports = mongoose.model('Member', memberSchema);
