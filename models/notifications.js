let mongoose = require('mongoose');

const oid = mongoose.Schema.Types.ObjectId;

let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 },
  strict: true
};

const notificationTypes = [
  'friend_request',
  'mention',
  'new_message',
  'new_clothes'
];

const notificationSchema = new mongoose.Schema({
  type: { type: String, enum: notificationTypes, required: true }
});

module.exports = mongoose.model('Notification', notificationSchema);
