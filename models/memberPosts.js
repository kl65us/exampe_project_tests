'use strict';

let mongoose = require('mongoose');
let Members = require('./members');

//posts that will be showed in feed and user profile
const memberPostSchema = new mongoose.Schema({
  memberId: { type: mongoose.Types.ObjectId },
  isReposted: { type: Boolean, default: false },
  repostedFrom: { type: mongooseTypes.ObjectId, ref: 'Member' }
});

module.exports = mongoose.model('MemberPost', memberPostSchema);
