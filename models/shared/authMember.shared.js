'use strict';
let bcrypt = require('bcrypt');

module.exports = exports = function (email, password, cb) {
  return this.findOne({ email }).select('-_id +hash').exec( (err, member) => {
    if (err) throw new Error(err);
    if (member) {
      let promise = new Promise( (resolve, reject) => {
        let result = bcrypt.compareSync(password, member.hash);
        if (result) {
          //delete sensitive information
          member = member.toObject();
          delete member.hash;

          resolve(member);
        } else reject(new Error('Incorrect credentials'));
      });

      cb(promise);
    } else {
      cb( Promise.reject(false) );
    }
  });
};
