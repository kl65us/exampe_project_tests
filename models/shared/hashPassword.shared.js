'use strict';
let bcrypt = require('bcrypt');

module.exports = exports = (hash) => {
  const saltRounds = 10;
  const password = hash;
  return new Promise( (resolve, reject) => bcrypt.hash(password, saltRounds, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    })
  );
};
