'use strict';

let mongoose = require('mongoose');

//TODO: check does it works
const currency = require('./shared/currency');

let options = {
  autoIndex: true,
  toJSON: { getters: true, virtuals: true },
  bufferCommands: true,
  id: false,
  minimize: false,
  safe: { j:1, w: 1, wtimeout: 5000 },
  strict: false
};

const Shop = {
  //TODO: create model schema for shops
};

//TODO: here we should think about image sizes
// do we need to resize a couple of images like
//
const itemPhoto = {
  src: { type: String, maxlength: 150, required: true },
  title: { type: String, maxlength: 50, required: true },
  description: { type: String, maxlength: 200, required: false, default: 'Not available' }
};

// !!!
// I think these field are related to Item model, not to wardrobe
// but wardrobe should contains links to shop's clothes (items)
const clothesSChema = new mongoose.Schema({
  // here we haven't ids 'cause it will be stored
  // inside user document as array of sub-documents

  size: { type: String, maxlength: 4, required: true },
  //TODO: find max length of color word
  color: { type: String, maxlength: 15, default: 'Not available' },
  //TODO: I guess with shops we should start working right now (db structure)
  // and interactions
  boughtAt: { type: mongoose.Types.ObjectId, ref: 'Shop' },
  //TODO: -1 = undefined or not set;
  price: { type: Number, maxlength: 10, required: false, default: -1 },
  //TODO: what we should do with currency if price may ot may not be specified?
  currency: { type: String, enum: currency, default: 'USD', required: true },
  // it can be either brand name or just item name like sneakers etc
  brandTitle: { type: String, maxlength: 100, required: true },
  itemDescription: { type: String, maxlength: 500, required: false },
  //TODO: write method for changing main item photo
  //if we separace main image we reduce calculation on the front-end
  itemMainPhoto: itemPhoto
  itemPhotos: [ itemPhoto ]

  //we do not need comments as sub-docs
  // 'cause comments will be revealed after click on comment icon in app
  comments: [{ type: mongoose.Types.ObjectId, ref: 'Comment'}]
});

module.exports = mongoose.model('Clothes', clothesSChema);
