'use strict';

let mongoose = require('mongoose');

module.exports.createMember = function (memberObj, cb) {
  return this.create( memberObj, cb );
};

module.exports.getAllFriends = function (userId, cb) {
  return this
            .findOne()
            .where('_id').eq(userId)
            .select('friends')
            .populate({
              path: 'friends',
              select: 'firstname lastname avatar'
            })
            .exec( cb );
};

module.exports.addFriend = function ( userId, friendId, cb ) {
  const updateQuery = {
    $push: { friends: mongoose.Types.ObjectId( friendId ) }
  };

  return this.findByIdAndUpdate( userId, updateQuery, { new: true } ).exec( cb );
};

module.exports.removeFriend = function ( userId, friendId, cb ) {
  const updateQuery = {
    $pull: { friends: friendId }
  };

  return this.update( {}, updateQuery, cb );
};
