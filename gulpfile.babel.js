'use strict';

let gulp = require('gulp');
let reqDir = require('requiredir');
let path = require('path');
let config = require('./gulp/config');

reqDir(path.join(__dirname, './gulp'), { recurse: true });


// gulp.task('build', ['clean:tmp', 'index', 'bundle', 'assets:js', 'templates', 'images', 'assets:plugins', 'less', 'locales'], () => {
// });

/* Default task that runs build process */
gulp.task('default', ['watch'], () => {
  //nothing here
});
