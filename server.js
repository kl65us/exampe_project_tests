'use strict';

require('babel-register');

let path = require('path');
let http = require('http');
let fs = require('fs');
let app = require('./app');

/* konfig initialization */
global.cf = require('konfig')({ path: path.join(__dirname, 'config') });
let debug = require('debug')(cf.app.name + ':server');

/* server creation */
let server = http.createServer(app).listen(cf.app.port);

module.exports = server;

/* error handling */
server
  .on('error', onError)
  .on('listening', onListening);

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof cf.app.port === 'string'
    ? 'Pipe ' + cf.app.port
    : 'Port ' + cf.app.port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}
