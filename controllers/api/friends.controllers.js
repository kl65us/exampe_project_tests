'use strict';

let mongoose = require('mongoose');
let Members = require('../../models/members');

//TODO: we should check req.token to let users get data or prevent data lack
module.exports.getAllFriends = (req, res, next) => {
  let { userId } = req.params;
  let response = res.locals.predefinedResponse;

  Members.getAllFriends( userId, (err, doc) => {
    if (err) return next(err);
    if (doc.friends) {
      response.data = doc.friends;
    } else {
      response.error = {
        message: "User haven't friends yet"
      };
    }

    res.json( response );
  });
};

//TODO: check if user exists
module.exports.addFriend = (req, res, next) => {
  let { userId } = req.params;
  let { friendId } = req.body;
  let response = res.locals.predefinedResponse;

  Members.findByIdAndUpdate( userId, friendId ).exec( (err, isUpdated) => {
    if (err) return next(err);
    response.data = {
      success: !!isUpdated
    };

    res.json( response );
  });
};

module.exports.removeFriend = (req, res, next) => {
  let { userId } = req.params;
  let { friendId } = req.body;
  let response = res.locals.predefinedResponse;

  Members.removeFriend( userId, friendId ).exec( (err) => {
    if (err) return next(err);

    //'cause if err exists that we have an error
    response.data = {
      success: true
    };

    res.json( response );
  });
};
