'use strict';

let jwt = require('jsonwebtoken');
let cf = require('konfig')();
let img = require('lwip');
let fs = require('fs');

module.exports.get = (req, res, next) => {
  let response = res.locals.predefinedResponse;
  let fileName = req.params.fileName;

  fs.readFile( `./uploads/${fileName.trim()}`, (err, image) => {
    if (err || !image) {
      let error = new Error('Error getting image');
      error = Object.assign( error, err );
      response.error = error;
      res.json( response );
    }

    //set headers
    let headers = {
      'Content-Type': 'application/octet-stream',
      'Content-disposition': `attachment; filename=${fileName}`
    };

    res.set( headers );
    res.end( image );
  });
};

module.exports.upload = (req, res, next) => {
  let response = res.locals.predefinedResponse;
  let { w:width, h:height, s:scale, t:top, l:left, r:right, b:bottom } = req.query;
  let file = req.file;

  img.open( file.path, (err, image) => {
    const error = new Error( 'Processing error' );

    if (err || !image) {
      response.error = Object.assign( error, err );
      return res.json( response );
    }

    try {
      image.batch()
      .scale( +scale )
      .crop( +left, +top, +right, +bottom )
      .writeFile( file.path, err => {
        if (err) {
          throw new Error(err);
        }

        response.data = {
          status: true,
          filename: file.filename
        };

        res.json( response );
      });
    } catch( err ) {
      fs.stat( file.path, (err, stats) => {
        if (!err && stats && stats.isFile()) {
          fs.unlink( file.path);
        }
      });

      response.error = {
        message: err.message
      };

      res.status(404).json( response );
    }

  });
};
