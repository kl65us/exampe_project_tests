'use strict';

let Members = require('../../models/members');

let jwt = require('jsonwebtoken');
let cf = require('konfig')();

module.exports.confirm = (req, res, next) => {
  let response = res.locals.predefinedResponse;
  let { confirmCode, newPass, confirmNewPass } = req.body || {};

  Members.findOne({ 'confirm_code': confirmCode }, (err, member) => {
    if (err) next(err);
    setNewPass(member);
  });

  function setNewPass(user) {
    if (user) {
      Members.hashPassword(newPass).then(hash => {
        user.hash = hash;
        user.save( err => {
          if (err) next(err);
          // response.data = { status: 'changed' };
          // res.json(response);
          res.sendStatus(200);
        });
      });
    } else {
      res.sendStatus(404);
    }

  }
};
