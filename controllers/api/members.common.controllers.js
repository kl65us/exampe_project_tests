'use strict';

let Members = require('../../models/members');

module.exports.all = (req, res, next) => {
  let response = res.locals.predefinedResponse;

  Members
    .find()
    .where('is_deleted').ne(true)
    .select('-_id -introduction -notifications -balance -is_deleted -created_at -token')
    .exec( (err, members) => {
      if (err) return next(err);
      response.data = members;
      res.status(200).json(response);
    });
};

module.exports.canRestore = (req, res, next) => {
  //TODO: here should be permisions only for staff, not for users
  let response = res.locals.predefinedResponse;

  Members
    .find()
    .where('is_deleted').eq(true)
    .select('-_id -introduction -notifications -is_deleted -token')
    .exec( (err, deletedMembers) => {
      if (err) return next(err);
      response.data = deletedMembers;
      res.status(200).json(response);
    });
};
