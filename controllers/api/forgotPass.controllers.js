'use strict';

let Members = require('../../models/members');

let jwt = require('jsonwebtoken');
let cf = require('konfig')();

module.exports.sendEmail = (req, res, next) => {
  let response = res.locals.predefinedResponse;
  let { email } = req.body || {};

  Members.findOne({ email }, (err, member) => {
    if (err) next(err);
    console.log('MEMBER', member);
    setConfirmCode(member);
  });

  function setConfirmCode(user) {
    if (user) {
      let code = Math.random().toString(36).substr(2, 5);
      console.log('CODE', code);
      user.confirm_code = code;
      user.save( err => {
        if (err) return next(err);
        // TODO: should send email
        res.sendStatus(200);
      });
    } else {
      res.sendStatus(404);
    }
  }
};
